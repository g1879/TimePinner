# 简介

TimePinner 是一个简单的计时工具。

类似于代码中的秒表。

可标记多个点，以记录若干段时间长度。

每段时间可以命名，以方便记忆，也可跳过无须记录的时间段。

# 安装与导入

## 安装

```python
pip install TimePinner
```

## 导入

```python
from TimePinner import Pinner
```

# 使用方法

## 创建对象

```python
from TimePinner import Pinner

pinner = Pinner()
```

## 记录时间点

使用`pin()`方法记录一个时间点，可以通过`text`参数给该时间点命名。

记录的时候会把当前时间段打印出来，可用`show()`参数设置不打印。

也可以在创建对象时用`show_everytime`参数设置是否打印。

`pin()`的`show`参数比创建对象时`show_everytime`参数优先级高。

```python
pinner = Pinner()
pinner.pin()  # 记录起始点
sleep(1)
pinner.pin('记录1')
sleep(2)
pinner.pin('记录2', show=False)  # 不打印该节点
```

输出：

```console
0.0
记录1：1.0084643
```

## 跳过时间段

有些时间段无须记录，用`skip()`方法跳过。下一个记录会以当前点作为起始。

```python
pinner.skip()
```

## 打印结果

通过`show()`方法，可以把记录到的时间段打印出来。

```console
记录1：1.007383
记录2：2.0145351000000002
```

## 获取结果

通过`records`属性获取各个记录的时间段，每个时间段为一个两位元组。

```python
print(pinner.records)
```

输出：

```console
[('记录1', 1.0084643), ('记录2', 2.0126551999999998)]
```

## 获取最短的时间段

通过`winner`属性可以获取最短的时间段，用于对比几段代码运行时间。

```python
print(pinner.winner)
```

输出：

```console
('记录1', 1.0084643)
```
